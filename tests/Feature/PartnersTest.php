<?php

namespace Tests\Feature;

use App\Models\Partner;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PartnersTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetPartners(): void
    {
        Partner::factory(2)->create();
        $response = $this->get(route('partners.list'));
        $response->assertOk();
        $response->assertJsonStructure([
            '*' => [
                'id',
                'title',
                'description',
                'image_url'
            ]
        ]);
        $data = $response->json();
        $this->assertNotEmpty($data);
        $this->assertCount(2, $data);
    }
}
