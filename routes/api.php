<?php

use App\Http\Controllers\CommitteeMemberController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PartnerController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'news',
    'as' => 'news.'
], function () {
    Route::get('/', [NewsController::class, 'index'])->name('list');
    Route::get('{slug}', [NewsController::class, 'show'])->name('show');
});

Route::group([
    'prefix' => 'partners',
    'as' => 'partners.'
], function () {
    Route::get('/', [PartnerController::class, 'index'])->name('list');
});

Route::group([
    'prefix' => 'committees',
    'as' => 'committees.'
], function () {
    Route::get('/', [CommitteeMemberController::class, 'index'])->name('list');
});