<?php

namespace App\Http\Resources;

use App\Models\CommitteeGroup;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'CommitteeGroupResource',
    properties: [
        new OA\Property(
            property: 'id',
            description: 'ID',
            type: 'int',
            example: 1
        ),
        new OA\Property(
            property: 'name',
            description: 'Наименование',
            type: 'string',
            example: 'Руководство'
        ),
        new OA\Property(
            property: 'members',
            type: 'array',
            items: new OA\Items(
                ref: "#/components/schemas/CommitteeMemberResource",
            )
        ),
    ]
)]
/** @mixin CommitteeGroup */
class CommitteeGroupResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'members' => CommitteeMemberResource::collection($this->committeeMembers->sortBy('order'))
        ];
    }
}
