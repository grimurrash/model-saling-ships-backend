<?php

namespace App\Http\Resources\News;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'NewsInfoResource',
    properties: [
        new OA\Property(
            property: 'slug',
            description: 'Slug новости, используется в ссылке',
            type: 'string',
            example: 'novosti-goroda'
        ),
        new OA\Property(
            property: 'short_title',
            description: 'Кратное оглавление новости',
            type: 'string',
            example: 'Новости города'
        ),
        new OA\Property(
            property: 'short_description',
            description: 'Краткое описание новости',
            type: 'string',
            example: 'Краткое описание новости'
        ),
        new OA\Property(
            property: 'title',
            description: 'Оглавление новости',
            type: 'string',
            example: 'Новости города'
        ),
        new OA\Property(
            property: 'description',
            description: 'Описание новости',
            type: 'string',
            example: 'Описание новости'
        ),
        new OA\Property(
            property: 'preview_image_url',
            description: 'Ссылка на превью изображение новости',
            type: 'string',
            example: 'https://project-ships.iaspatriot.ru/storage/images/novosti-goroda.png'
        ),
        new OA\Property(
            property: 'video_url',
            description: 'Ссылка на видео новости',
            type: 'string',
            example: 'https://www.youtube.com/watch?v=FVDKDH1Yojs'
        ),
        new OA\Property(
            property: 'document_url',
            description: 'Ссылка на документ новости',
            type: 'string',
            example: 'https://project-ships.iaspatriot.ru/storage/documents/novosti-goroda.pdf'
        ),
        new OA\Property(
            property: 'date',
            description: 'Дата новости',
            type: 'string',
            example: '2023-12-11 14:01:01'
        ),
    ]
)]
/** @mixin News */
class NewsInfoResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'short_title' => $this->short_title,
            'short_description' => $this->short_description,
            'title' => $this->title,
            'description' => $this->description,
            'preview_image_url' => Storage::disk('public')->url($this->preview_image_url),
            'video_url' => isset($this->video_url) ? Storage::disk('public')->url($this->video_url) : null,
            'document_url' => isset($this->video_url) ? Storage::disk('public')->url($this->document_url) : null,
            'date' => $this->date->toDateTimeString(),
        ];
    }
}
