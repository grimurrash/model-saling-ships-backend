<?php

namespace App\Http\Resources\News;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'NewsListResource',
    properties: [
        new OA\Property(
            property: 'items',
            type: 'array',
            items: new OA\Items(
                ref: "#/components/schemas/NewsListItemResource",
            )
        ),
        new OA\Property(
            property: 'meta',
            properties: [
                new OA\Property(
                    property: 'current_page',
                    description: 'Нынешная страница',
                    type: 'integer',
                    example: 1
                ),
                new OA\Property(
                    property: 'last_page',
                    description: 'Последная возможная страница c нынешним кол-вом новостей на странице',
                    type: 'integer',
                    example: 2
                ),
                new OA\Property(
                    property: 'per_page',
                    description: 'Кол-во новостей на 1-ой странице',
                    type: 'integer',
                    example: 5
                ),
                new OA\Property(
                    property: 'total',
                    description: 'Общее кол-во новостей',
                    type: 'integer',
                    example: 7
                ),
                new OA\Property(
                    property: 'from_item',
                    description: 'Номер первой новости на данной странице',
                    type: 'integer',
                    example: 1,
                    nullable: true
                ),
                new OA\Property(
                    property: 'to_item',
                    description: 'Номер последней новости на данной странице',
                    type: 'integer',
                    example: 5,
                    nullable: true
                ),
            ]
        ),
    ]
)]
/** @property LengthAwarePaginator|Collection $resource */
class NewsListResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        if ($this->resource instanceof LengthAwarePaginator) {
            return [
                'items' => NewsListItemResource::collection($this->resource->items()),
                'meta' => [
                    'current_page' => $this->resource->currentPage(),
                    'per_page' => $this->resource->perPage(),
                    'last_page' => $this->resource->lastPage(),
                    'total' => $this->resource->total(),
                    'from_item' => $this->resource->firstItem(),
                    'to_item' => $this->resource->lastItem(),
                ]
            ];
        }

        return [
            'items' => NewsListItemResource::collection($this->resource),
        ];
    }
}
