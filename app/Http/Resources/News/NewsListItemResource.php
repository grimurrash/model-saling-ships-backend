<?php

namespace App\Http\Resources\News;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'NewsListItemResource',
    properties: [
        new OA\Property(
            property: 'slug',
            description: 'Slug новости, используется в ссылке',
            type: 'string',
            example: 'novosti-goroda'
        ),
        new OA\Property(
            property: 'short_title',
            description: 'Кратное оглавление новости',
            type: 'string',
            example: 'Новости города'
        ),
        new OA\Property(
            property: 'short_description',
            description: 'Краткое описание новости',
            type: 'string',
            example: 'Краткое описание новости'
        ),
        new OA\Property(
            property: 'preview_image_url',
            description: 'Ссылка на превью изображение новости',
            type: 'string',
            example: 'https://project-ships.iaspatriot.ru/storage/images/novosti-goroda.png'
        ),
        new OA\Property(
            property: 'date',
            description: 'Дата новости',
            type: 'string',
            example: '2023-12-11 14:01:01'
        ),
    ]
)]
/** @mixin News */
class NewsListItemResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'slug' => $this->slug,
            'short_title' => $this->short_title,
            'short_description' => $this->short_description,
            'preview_image_url' => Storage::disk('public')->url($this->preview_image_url),
            'date' => $this->date->toDateTimeString(),
        ];
    }
}
