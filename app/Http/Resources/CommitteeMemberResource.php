<?php

namespace App\Http\Resources;

use App\Models\CommitteeMember;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'CommitteeMemberResource',
    properties: [
        new OA\Property(
            property: 'id',
            description: 'ID',
            type: 'int',
            example: 1
        ),
        new OA\Property(
            property: 'full_name',
            description: 'ФИО',
            type: 'string',
            example: 'ФИО'
        ),
        new OA\Property(
            property: 'title',
            description: 'Оглавление',
            type: 'string',
            example: 'Судья международной категории СМК'
        ),
        new OA\Property(
            property: 'description',
            description: 'Описание',
            type: 'string',
            example: 'Президент Федерации судомодельного спорта.'
        ),
        new OA\Property(
            property: 'image_url',
            description: 'Ссылка на изображение',
            type: 'string',
            example: 'https://project-ships.iaspatriot.ru/storage/committee/1.png'
        ),
    ]
)]
/** @mixin CommitteeMember */
class CommitteeMemberResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'image_url' => Storage::disk('public')->url($this->image_url),
            'title' => $this->title,
            'description' => $this->description,
        ];
    }
}
