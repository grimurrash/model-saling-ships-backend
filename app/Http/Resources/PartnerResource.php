<?php

namespace App\Http\Resources;

use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;
use OpenApi\Attributes as OA;

#[OA\Schema(
    schema: 'PartnerResource',
    properties: [
        new OA\Property(
            property: 'id',
            description: 'ID партнера',
            type: 'int',
            example: 1
        ),
        new OA\Property(
            property: 'title',
            description: 'Наименование партнера',
            type: 'string',
            example: 'ВТБ'
        ),
        new OA\Property(
            property: 'description',
            description: 'Описание партнера',
            type: 'string',
            example: 'Банк номер 1'
        ),
        new OA\Property(
            property: 'image_url',
            description: 'Ссылка на эмблему партнера',
            type: 'string',
            example: 'https://project-ships.iaspatriot.ru/storage/images/novosti-goroda.png'
        ),
    ]
)]
/** @mixin Partner */
class PartnerResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image_url' => Storage::disk('public')->url($this->image_url),
            'site_url' => $this->site_url
        ];
    }
}
