<?php

namespace App\Http\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Http\Requests\GetNewsListRequest;
use App\Http\Resources\News\NewsInfoResource;
use App\Http\Resources\News\NewsListResource;
use App\Models\News;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use OpenApi\Attributes as OA;

class NewsController extends Controller
{
    #[OA\Get(
        path: '/api/news',
        description: 'Получения списка новостей',
        tags: ['Новости'],
        parameters: [
            new OA\Parameter(
                name: 'page',
                description: 'Страница',
                in: 'query',
                schema: new OA\Schema(
                    type: 'integer',
                    default: 1
                ),
                example: 1,
            ),
            new OA\Parameter(
                name: 'per_page',
                description: 'Кол-во новостей в 1 странице',
                in: 'query',
                schema: new OA\Schema(
                    type: 'integer',
                    default: 5
                ),
                example: 5,
            ),
            new OA\Parameter(
                name: 'is_all',
                description: 'Показать все новости',
                in: 'query',
                schema: new OA\Schema(
                    type: 'bool',
                    default: 0
                ),
                example: 0,
            ),
            new OA\Parameter(
                name: 'date',
                description: 'Дата новости',
                in: 'query',
                schema: new OA\Schema(
                    type: 'date',
                    default: '2000-12-12'
                ),
                example: '2000-12-12',
            ),
            new OA\Parameter(
                name: 'search',
                description: 'Поиск',
                in: 'query',
                schema: new OA\Schema(
                    type: 'string',
                    default: 'random'
                ),
                example: 'random',
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(
                    ref: '#/components/schemas/NewsListResource'
                )
            ),
        ]
    )]
    public function index(GetNewsListRequest $request): JsonResponse
    {
        $query = News::query()
            ->orderBy('date', 'desc')
            ->visible()
            ->when($request->get('search'), function (Builder $query, $searchText) {
                $query->where(function (Builder $query) use ($searchText) {
                    $query->where('description', 'like', "%$searchText%")
                        ->orWhere('title', 'like', "%$searchText%")
                        ->orWhere('short_title', 'like', "%$searchText%")
                        ->orWhere('short_description', 'like', "%$searchText%");
                });
            });
        if ($request->get('is_all', false)) {
            $news = $query->get();
        } else {
            $news = $query->paginate(
                perPage: $request->get('per_page', 5),
                page: $request->get('page', 1)
            );
        }

        return response()->json(NewsListResource::make($news));
    }

    /**
     * @throws EntityNotFoundException
     */
    #[OA\Get(
        path: '/api/news/{slug}',
        description: 'Получения полных данных о новости по slug',
        tags: ['Новости'],
        parameters: [
            new OA\Parameter(
                name: 'slug',
                description: 'Slug новости',
                in: 'path',
                schema: new OA\Schema(
                    type: 'string',
                ),
                example: 'novosti-goroda',
            ),
        ],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(
                    ref: '#/components/schemas/NewsInfoResource'
                )
            ),
            new OA\Response(
                response: 404,
                description: 'Ответ если новость не найдена',
                content: new OA\JsonContent(
                    ref: '#/components/schemas/EntityNotFoundException'
                )
            ),
        ]
    )]
    public function show(string $slug): JsonResponse
    {
        $news = News::query()
            ->where('slug', $slug)
            ->visible()
            ->first();

        if (is_null($news)) {
            throw new EntityNotFoundException('Новость не найдена');
        }

        return response()->json(NewsInfoResource::make($news));
    }
}
