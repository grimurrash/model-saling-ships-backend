<?php

namespace App\Http\Controllers;

use App\Http\Resources\PartnerResource;
use App\Models\Partner;
use Illuminate\Http\JsonResponse;
use OpenApi\Attributes as OA;

class PartnerController extends Controller
{
    #[OA\Get(
        path: '/api/partners',
        description: 'Получения списка партнеров',
        tags: ['Партнеры'],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(
                    type: 'array',
                    items: new OA\Items(
                        ref: '#/components/schemas/PartnerResource'
                    ),
                )
            ),
        ]
    )]
    public function index(): JsonResponse
    {
        $partners = Partner::query()
            ->visible()
            ->orderBy('order')
            ->get();

        return response()->json(PartnerResource::collection($partners));
    }

}
