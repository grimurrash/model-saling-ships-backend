<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommitteeGroupResource;
use App\Models\CommitteeGroup;
use Illuminate\Http\JsonResponse;
use OpenApi\Attributes as OA;

class CommitteeMemberController extends Controller
{
    #[OA\Get(
        path: '/api/committees',
        description: 'Получения списка членов комитета/судей',
        tags: ['Комитет'],
        responses: [
            new OA\Response(
                response: 200,
                description: 'OK',
                content: new OA\JsonContent(
                    type: 'array',
                    items: new OA\Items(
                        ref: '#/components/schemas/CommitteeGroupResource'
                    ),
                )
            ),
        ]
    )]
    public function index(): JsonResponse
    {
        $committeeMembers = CommitteeGroup::query()
            ->visible()
            ->withWhereHas('committeeMembers', function ($query) {
                $query->visible();
            })
            ->orderBy('order')
            ->get();

        return response()->json(CommitteeGroupResource::collection($committeeMembers));
    }
}
