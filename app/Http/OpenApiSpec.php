<?php

namespace App\Http;

use OpenApi\Attributes as OA;

#[OA\Info(
    version: '1.0.0',
    title: 'Api documentation для сайта',
)]
#[OA\Server(
    url: '/',
    description: 'Local'
)]
#[OA\Server(
    url: 'https://project-ships.iaspatriot.ru/',
    description: 'Prod'
)]
class OpenApiSpec
{

}
