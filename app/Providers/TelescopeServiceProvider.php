<?php

namespace App\Providers;

use Laravel\Telescope\IncomingEntry;
use Laravel\Telescope\Telescope;
use Laravel\Telescope\TelescopeApplicationServiceProvider;

class TelescopeServiceProvider extends TelescopeApplicationServiceProvider
{
    public function register(): void
    {
        Telescope::night();

        $this->hideSensitiveRequestDetails();

        Telescope::tag(function (IncomingEntry $entry) {
            $tags = [];
            if ($entry->type === 'request') {
                $tags = [
                    'status:' . $entry->content['response_status'],
                    'host:' . $entry->content['headers']['host'],
                    'url:' . $entry->content['uri'],
                    'method:' . $entry->content['method'],
                ];
                if ($entry->content['duration'] > 100) {
                    $tags[] = 'is_slow';
                }
            }

            return $tags;
        });

        Telescope::filter(function (IncomingEntry $entry) {
            return $entry->isRequest()
                || $entry->isFailedJob()
                || $entry->isSlowQuery()
                || $entry->isScheduledTask()
                || $entry->isClientRequest()
                || $entry->hasMonitoredTag()
                || $entry->isException();
        });
    }

    protected function hideSensitiveRequestDetails(): void
    {
        if ($this->app->environment('local')) {
            return;
        }

        Telescope::hideRequestParameters(['_token']);

        Telescope::hideRequestHeaders([
            'cookie',
            'x-csrf-token',
            'x-xsrf-token',
        ]);
    }

    protected function gate(): void
    {
//        Gate::define('viewTelescope', function (User $user) {
//            return $user->role->isRoot();
//        });
    }
}
