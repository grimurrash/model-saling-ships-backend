<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;
use Throwable;
use OpenApi\Attributes as OA;
#[OA\Schema(
    schema: 'EntityNotFoundException',
    properties: [
        new OA\Property(
            property: 'error',
            description: 'Текст ошибки',
            type: 'string',
            example: 'Новость не найдена'
        ),
    ]
)]
class EntityNotFoundException extends Exception
{
    public function __construct(string $message = "", int $code = 404, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function render(): JsonResponse
    {
        return response()->json([
            'error' => $this->getMessage()
        ], 404);
    }
}
