<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CommitteeGroup extends Model
{
    use HasFactory;

    protected $table = 'committee_groups';

    public function scopeVisible(Builder $query): Builder
    {
        return $query->where('is_visible', true);
    }

    public function committeeMembers(): HasMany
    {
        return $this->hasMany(CommitteeMember::class, 'group_id');
    }
}
