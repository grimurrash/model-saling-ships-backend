<?php

namespace App\Filament\Resources\CommitteeMemberResource\Pages;

use App\Filament\Resources\CommitteeMemberResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCommitteeMember extends CreateRecord
{
    protected static string $resource = CommitteeMemberResource::class;
}
