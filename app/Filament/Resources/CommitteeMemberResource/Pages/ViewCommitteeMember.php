<?php

namespace App\Filament\Resources\CommitteeMemberResource\Pages;

use App\Filament\Resources\CommitteeGroupResource;
use App\Filament\Resources\CommitteeMemberResource;
use App\Models\CommitteeGroup;
use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;

class ViewCommitteeMember extends ViewRecord
{
    protected static string $resource = CommitteeMemberResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
