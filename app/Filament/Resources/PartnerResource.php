<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PartnerResource\Pages;
use App\Models\Partner;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class PartnerResource extends Resource
{
    protected static ?string $model = Partner::class;

    protected static ?string $label = 'Партнер';

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('title')
                    ->label('Наименование партнера')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Textarea::make('description')
                    ->label('Описание партнера')
                    ->required()
                    ->maxLength(65535)
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('site_url')
                    ->label('Ссылка на сайт партнера')
                    ->required()
                    ->url()
                    ->activeUrl()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Toggle::make('is_visible')
                    ->required()
                    ->label('Показывать'),
                Forms\Components\TextInput::make('order')
                    ->required()
                    ->numeric()
                    ->label('Порядок'),
                Forms\Components\FileUpload::make('image_url')
                    ->label('Изобращение на превью')
                    ->image()
                    ->required()
                    ->avatar()
                    ->disk('public')
                    ->directory('partners/images')
                    ->visibility('public')
                    ->imageEditor()
                    ->previewable()
                    ->imageEditorAspectRatios([
                        '16:9',
                        '4:3',
                        '1:1',
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->sortable(),
                Tables\Columns\ImageColumn::make('image_url')
                    ->label('Эмблема партнера')
                    ->toggleable(),
                Tables\Columns\TextColumn::make('title')
                    ->label('Наименование партнера')
                    ->searchable(),
                Tables\Columns\TextColumn::make('site_url')
                    ->label('Ссылка на сайт партнера'),
                Tables\Columns\TextColumn::make('order')
                    ->numeric()
                    ->label('Порядок')
                    ->sortable()
                    ->toggleable(),
                Tables\Columns\CheckboxColumn::make('is_visible')
                    ->label('Показывать')
                    ->disabled()
                    ->toggleable(),
            ])
            ->filters([
                Tables\Filters\Filter::make('is_visible')
                    ->label('Показывать')
                    ->toggle()
            ])
            ->defaultSort('order')
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPartners::route('/'),
            'create' => Pages\CreatePartner::route('/create'),
            'view' => Pages\ViewPartner::route('/{record}'),
            'edit' => Pages\EditPartner::route('/{record}/edit'),
        ];
    }
}
