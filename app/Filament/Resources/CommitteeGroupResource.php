<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CommitteeGroupResource\Pages;
use App\Filament\Resources\CommitteeGroupResource\RelationManagers;
use App\Models\CommitteeGroup;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class CommitteeGroupResource extends Resource
{
    protected static ?string $model = CommitteeGroup::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $label = 'Комитета';

    protected static ?string $navigationGroup = 'Комитеты';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->label('Наименование')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Toggle::make('is_visible')
                    ->required()
                    ->default(true)
                    ->label('Показывать'),
                Forms\Components\TextInput::make('order')
                    ->numeric()
                    ->required()
                    ->label('Порядок'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
                    ->label('Наименование')
                    ->searchable(),
                Tables\Columns\TextColumn::make('committee_members_count')
                    ->label('Кол-во участников')
                    ->counts('committeeMembers'),
                Tables\Columns\TextColumn::make('order')
                    ->numeric()
                    ->label('Порядок')
                    ->sortable()
                    ->toggleable(),

                Tables\Columns\CheckboxColumn::make('is_visible')
                    ->label('Показывать')
                    ->disabled()
                    ->toggleable(),
            ])
            ->filters([
                Tables\Filters\Filter::make('is_visible')
                    ->label('Показывать')
                    ->toggle()
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->defaultSort('order')
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            RelationManagers\CommitteeMembersRelationManager::class
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCommitteeGroups::route('/'),
            'create' => Pages\CreateCommitteeGroup::route('/create'),
            'view' => Pages\ViewCommitteeGroup::route('/{record}'),
            'edit' => Pages\EditCommitteeGroup::route('/{record}/edit'),
        ];
    }
}
