<?php

namespace App\Filament\Resources;

use App\Filament\Resources\CommitteeMemberResource\Pages;
use App\Models\CommitteeMember;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class CommitteeMemberResource extends Resource
{
    protected static ?string $model = CommitteeMember::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    protected static ?string $label = 'Члены Комитета';

    protected static ?string $navigationGroup = 'Комитеты';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('group_id')
                    ->relationship('group', 'name')
                    ->searchable()
                    ->preload()
                    ->columnSpanFull()
                    ->label('Комитет')
                    ->required(),
                Forms\Components\TextInput::make('full_name')
                    ->label('ФИО')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\TextInput::make('title')
                    ->label('Направление')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Textarea::make('description')
                    ->label('Краткое описание')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Toggle::make('is_visible')
                    ->required()
                    ->default(true)
                    ->label('Показывать'),
                Forms\Components\TextInput::make('order')
                    ->numeric()
                    ->required()
                    ->label('Порядок'),
                Forms\Components\FileUpload::make('image_url')
                    ->label('Изобращение на превью')
                    ->image()
                    ->required()
                    ->disk('public')
                    ->directory('partners/images')
                    ->visibility('public')
                    ->imageEditor()
                    ->columnSpanFull()
                    ->previewable()
                    ->imageEditorAspectRatios([
                        '16:9',
                        '4:3',
                        '1:1',
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('full_name')
                    ->label('ФИО')
                    ->searchable(),
                Tables\Columns\TextColumn::make('group.name')
                    ->label('Комитет'),
                Tables\Columns\TextColumn::make('title')
                    ->label('Направление'),
                Tables\Columns\CheckboxColumn::make('is_visible')
                    ->label('Показывать')
                    ->disabled()
                    ->toggleable(),
            ])
            ->filters([
                Tables\Filters\SelectFilter::make('group_id')
                    ->label('Комитет')
                    ->relationship('group', 'name')
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->defaultSort('order')
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCommitteeMembers::route('/'),
            'create' => Pages\CreateCommitteeMember::route('/create'),
            'view' => Pages\ViewCommitteeMember::route('/{record}'),
            'edit' => Pages\EditCommitteeMember::route('/{record}/edit'),
        ];
    }
}
