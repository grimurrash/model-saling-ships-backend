<?php

namespace App\Filament\Resources;

use App\Filament\Resources\NewsResource\Pages;
use App\Models\News;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Filters;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class NewsResource extends Resource
{
    protected static ?string $model = News::class;

    protected static ?string $label = 'Новости';

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('short_title')
                    ->label('Краткое оглавление')
                    ->required()
                    ->live()
                    ->maxLength(255)
                    ->afterStateUpdated(fn($state, callable $set) => $set('slug', Str::slug($state))),
                Forms\Components\TextInput::make('slug')
                    ->label('Путь в ссылке')
                    ->disabled()
                    ->required()
                    ->unique(News::class, 'slug', fn($record) => $record),
                Forms\Components\Textarea::make('short_description')
                    ->label('Краткое описание')
                    ->required()
                    ->maxLength(255)
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('title')
                    ->label('Полное оглавление')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\MarkdownEditor::make('description')
                    ->label('Тело новости')
                    ->maxLength(65535)
                    ->fileAttachmentsDisk('public')
                    ->fileAttachmentsDirectory('attachments')
                    ->fileAttachmentsVisibility('public')
                    ->disableToolbarButtons()
                    ->columnSpanFull(),
                Forms\Components\Toggle::make('is_visible')
                    ->label('Показывать'),
                Forms\Components\DateTimePicker::make('date')
                    ->label('Дата новости')
                    ->required()
                    ->default(now()),
                Forms\Components\Section::make('Файлы')
                    ->schema([
                        Forms\Components\FileUpload::make('preview_image_url')
                            ->label('Изобращение на превью')
                            ->image()
                            ->required()
                            ->disk('public')
                            ->directory('news/images')
                            ->visibility('public')
                            ->imageEditor()
                            ->previewable()
                            ->imageEditorAspectRatios([
                                '16:9',
                                '4:3',
                                '1:1',
                            ]),
                        Forms\Components\FileUpload::make('video_url')
                            ->label('Прикрепленное видео')
                            ->disk('public')
                            ->directory('news/videos')
                            ->visibility('public'),
                        Forms\Components\FileUpload::make('document_url')
                            ->label('Прикрепленный документ')
                            ->disk('public')
                            ->directory('news/documents')
                            ->visibility('public'),
                    ]),

            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('slug')
                    ->label('Путь в ссылке')
                    ->searchable(isIndividual: true),
                Tables\Columns\TextColumn::make('short_title')
                    ->label('Краткое оглавление')
                    ->searchable(isIndividual: true)
                    ->toggledHiddenByDefault(false)
                    ->toggleable(),
                Tables\Columns\TextColumn::make('short_title')
                    ->label('Полное оглавление')
                    ->searchable(isIndividual: true)
                    ->toggleable(),
                Tables\Columns\TextColumn::make('date')
                    ->label('Дата новости')
                    ->searchable(isIndividual: true)
                    ->toggleable(),
                Tables\Columns\CheckboxColumn::make('is_visible')
                    ->label('Показывать')
                    ->disabled()
                    ->toggleable(),
            ])
            ->filters([
                Tables\Filters\Filter::make('is_visible')
                    ->label('Показывать')
                    ->toggle()
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListNews::route('/'),
            'create' => Pages\CreateNews::route('/create'),
            'view' => Pages\ViewNews::route('/{record}'),
            'edit' => Pages\EditNews::route('/{record}/edit'),
        ];
    }
}
