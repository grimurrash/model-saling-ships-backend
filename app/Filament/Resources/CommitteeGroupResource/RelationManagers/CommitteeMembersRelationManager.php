<?php

namespace App\Filament\Resources\CommitteeGroupResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Table;

class CommitteeMembersRelationManager extends RelationManager
{
    protected static string $relationship = 'committeeMembers';

    protected static ?string $label = 'Члены комитета';

    public function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('full_name')
                    ->label('Наименование')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\TextInput::make('title')
                    ->label('Направление')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Textarea::make('description')
                    ->label('Краткое описание')
                    ->required()
                    ->columnSpanFull()
                    ->maxLength(255),
                Forms\Components\Toggle::make('is_visible')
                    ->required()
                    ->default(true)
                    ->label('Показывать'),
                Forms\Components\TextInput::make('order')
                    ->numeric()
                    ->required()
                    ->label('Порядок'),
                Forms\Components\FileUpload::make('image_url')
                    ->label('Изобращение на превью')
                    ->image()
                    ->required()
                    ->disk('public')
                    ->directory('partners/images')
                    ->visibility('public')
                    ->imageEditor()
                    ->columnSpanFull()
                    ->previewable()
                    ->imageEditorAspectRatios([
                        '16:9',
                        '4:3',
                        '1:1',
                    ]),
            ]);
    }

    public function table(Table $table): Table
    {
        return $table
            ->recordTitleAttribute('full_name')
            ->columns([
                Tables\Columns\TextColumn::make('full_name')
                    ->label('ФИО')
                    ->searchable(),
                Tables\Columns\TextColumn::make('title')
                    ->label('Направление'),
                Tables\Columns\CheckboxColumn::make('is_visible')
                    ->label('Показывать')
                    ->disabled()
                    ->toggleable(),
            ])
            ->filters([
                //
            ])
            ->headerActions([
                Tables\Actions\CreateAction::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->defaultSort('order')
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }
}
