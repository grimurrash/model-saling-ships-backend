<?php

namespace App\Filament\Resources\CommitteeGroupResource\Pages;

use App\Filament\Resources\CommitteeGroupResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCommitteeGroups extends ListRecords
{
    protected static string $resource = CommitteeGroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
