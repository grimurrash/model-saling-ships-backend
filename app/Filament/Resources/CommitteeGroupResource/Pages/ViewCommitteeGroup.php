<?php

namespace App\Filament\Resources\CommitteeGroupResource\Pages;

use App\Filament\Resources\CommitteeGroupResource;
use App\Models\CommitteeGroup;
use Filament\Actions;
use Filament\Resources\Pages\ViewRecord;

class ViewCommitteeGroup extends ViewRecord
{
    protected static string $resource = CommitteeGroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\EditAction::make(),
        ];
    }
}
