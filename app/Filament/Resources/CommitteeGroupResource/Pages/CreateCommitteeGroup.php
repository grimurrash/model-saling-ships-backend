<?php

namespace App\Filament\Resources\CommitteeGroupResource\Pages;

use App\Filament\Resources\CommitteeGroupResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCommitteeGroup extends CreateRecord
{
    protected static string $resource = CommitteeGroupResource::class;
}
