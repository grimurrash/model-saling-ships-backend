<?php

namespace App\Filament\Resources\CommitteeGroupResource\Pages;

use App\Filament\Resources\CommitteeGroupResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCommitteeGroup extends EditRecord
{
    protected static string $resource = CommitteeGroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }

    protected function getRedirectUrl(): string
    {
        return self::getResource()::getUrl('view', ['record' => $this->getRecord()]);
    }
}
