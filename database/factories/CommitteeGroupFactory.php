<?php

namespace Database\Factories;

use App\Models\CommitteeGroup;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommitteeGroupFactory extends Factory
{
    protected $model = CommitteeGroup::class;

    public function definition(): array
    {
        return [

        ];
    }
}
