<?php

namespace Database\Factories;

use App\Models\CommitteeMember;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;

class CommitteeMemberFactory extends Factory
{
    protected $model = CommitteeMember::class;

    public function definition(): array
    {
        return [
            'full_name' => $this->faker->name(),
            'image_url' => $this->faker->imageUrl(),
            'category' => $this->faker->word(),
            'present_from' => $this->faker->word(),
            'description' => $this->faker->text(),
            'order' => 1,
            'is_visible' => true,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
    }
}
