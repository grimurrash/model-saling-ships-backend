<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('committee_members', function (Blueprint $table) {
            $table->id();

            $table->string('full_name');
            $table->string('image_url');
            $table->string('title')->nullable();
            $table->string('description')->nullable();

            $table->integer('order');
            $table->boolean('is_visible');

            $table->foreignId('group_id')->constrained('committee_groups');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('committee_members');
    }
};
